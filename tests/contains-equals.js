const { assert } = require('chai');
const { describe, it } = require('mocha');

const containsEquals = require('../functions/contains-equals.js')

module.exports = describe('#4 - Créer une fonction qui renvoi un element si il est égal à un autre', function () {
  it('should find the equality', function () {
    assert.equal(containsEquals([1, 4, 5, 10, 1]), 1)
    assert.equal(containsEquals([1, 3, 'test', 10, 'test']), 'test')
    assert.equal(containsEquals([1, true, 9, true, 'test']), true)
  });

  it('should return false', function () {
    assert.equal(containsEquals([]), false)
    assert.equal(containsEquals(null), false)
    assert.equal(containsEquals(undefined), false)
  });
});