const { expect, assert } = require('chai');
const { describe, it } = require('mocha');

const objectKeys = require('./../functions/object-keys')

module.exports = describe('#3 - Écrire une fonction qui retourne une liste des clés d\'un objet donné en paramètre', function() {
  it('should show an array of keys', function() {
    expect(objectKeys(
      { firstName: 'John', lastName: 'Legend', age: 20 }
    )).to.deep.equal(['firstName', 'lastName', 'age'])
    expect(objectKeys(
      { job: 'Ébéniste', department: 'Wood' }
    )).to.deep.equal(['job', 'department'])
  });

  it('should return', function() {
    assert.equal(objectKeys(['string','string','string']), false)
    assert.equal(objectKeys(null), false)
    assert.equal(objectKeys(undefined), false)
    assert.equal(objectKeys(''), false)
    assert.equal(objectKeys([]), false)
    assert.equal(objectKeys({}), false)
  });
});