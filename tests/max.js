const { assert } = require('chai');
const { describe, it } = require('mocha');

const maxDifference = require('./../functions/max')

module.exports = describe('#2 - Créer une fonction qui renvoie la plus grosse différence entre deux nombres à la suite dans un tableau', function() {
  it('should find the difference', function() {
    assert.equal(maxDifference([1,3,5,10,15]), 5)
    assert.equal(maxDifference([13,3,50,10,15]), 47)
    assert.equal(maxDifference([1,3,9,10,'test']), 6)
  });

  it('should return', function() {
    assert.equal(maxDifference(['string','string','string']), false)
    assert.equal(maxDifference(null), false)
    assert.equal(maxDifference(undefined), false)
    assert.equal(maxDifference(''), false)
    assert.equal(maxDifference([]), false)
  });
});