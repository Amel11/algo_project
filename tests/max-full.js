const { assert } = require('chai');
const { describe, it } = require('mocha');

const maxFullDifference = require('./../functions/max-full')

module.exports = describe('#2 - Créer une fonction qui renvoie la plus grosse différence entre les nombres d\'un tableau', function() {
  it('should find the difference', function() {
    assert.equal(maxFullDifference([1,3,5,10,15]), 14)
    assert.equal(maxFullDifference([13,3,50,10,15]), 47)
    assert.equal(maxFullDifference([1,3,9,10,'test']), 9)
  });

  it('should return', function() {
    assert.equal(maxFullDifference(['string','string','string']), false)
    assert.equal(maxFullDifference(null), false)
    assert.equal(maxFullDifference(undefined), false)
    assert.equal(maxFullDifference(''), false)
    assert.equal(maxFullDifference([]), false)
  });
});