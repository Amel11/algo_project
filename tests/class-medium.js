const { assert, expect } = require('chai');
const { describe, it } = require('mocha');

const classes = require('./../functions/class-medium')

module.exports = describe('#2 - Créer une classe vehicule avec deux attributs (model, price), faire hérité deux classes (Car, Truck) ayant chacun deux méthodes (start, speedUp)', function() {
  it('should be named correctly', function() {
    assert.equal((new classes.Vehicule()).constructor.name, 'Vehicule')
    assert.equal((new classes.Truck()).constructor.name, 'Truck')
    assert.equal((new classes.Car()).constructor.name, 'Car')
  });

  it('should have three attributes (matricule, model, price)', function() {
    expect(Object.getOwnPropertyNames(new classes.Vehicule())).to.eqls(['model', 'price'])
    expect(Object.getOwnPropertyNames(new classes.Truck())).to.eqls(['model', 'price'])
    expect(Object.getOwnPropertyNames(new classes.Car())).to.eqls(['model', 'price'])
  });

  it('should have two methods (start, speedUp)', function() {
    assert.equal(new classes.Truck().start(), 'Le camion démarre')
    assert.equal(new classes.Truck().speedUp(), 'Le camion accélére')
    assert.equal(new classes.Car().start(), 'La voiture démarre')
    assert.equal(new classes.Car().speedUp(), 'La voiture accélére')
  });

  it('should be instanciate with parameters', function() {
    const car = new classes.Car('Tesla', 60000)
    const truck = new classes.Truck('Man', 100000)

    assert.equal(car.model, 'Tesla')
    assert.equal(car.price, 60000)
    assert.equal(car.toString(), 'Model: Tesla, Price: 60000')


    assert.equal(truck.model, 'Man')
    assert.equal(truck.price, 100000)
    assert.equal(truck.toString(), 'Model: Man, Price: 100000')
  });
});