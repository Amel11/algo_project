String.prototype.truncate = function(word, position) {
    if (!word){
        return false
    }
    else {
        if(word.length<=position){
            return word
        }
        if(word[position -1] === " " || word[position-1] ===","){
            return word.substring(0, position-1) + "..."
        }
    }
    return word.substring(0, position) + "..."

}

module.exports = String.prototype.truncate